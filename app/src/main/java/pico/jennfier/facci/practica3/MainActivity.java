package pico.jennfier.facci.practica3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(MainActivity.this, ActividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, ActividadRegistrar.class);
                startActivity(intent);
                break;
            case R.id.opcionLinear:
                intent = new Intent(MainActivity.this, ActividadLinear.class);
                startActivity(intent);
                break;
            case R.id.opcionFrame:
                intent = new Intent(MainActivity.this, ActividadFrame.class);
                startActivity(intent);
                break;
            case R.id.opcionTable:
                intent = new Intent(MainActivity.this, ActividadTable.class);
                startActivity(intent);
                break;
            case R.id.opcionColores:
                intent = new Intent(MainActivity.this, ActividadColores.class);
                startActivity(intent);
                break;
            case R.id.opcionAcelerometro:
                intent = new Intent(MainActivity.this, ActividadAcelerometro.class);
                startActivity(intent);
                break;
            case R.id.opcionProximidad:
                intent = new Intent(MainActivity.this, ActividadProximidad.class);
                startActivity(intent);
                break;
            case R.id.opcionLuz:
                intent = new Intent(MainActivity.this, ActividadLuz.class);
                startActivity(intent);
                break;
            case R.id.opcionVibrar:
                intent = new Intent(MainActivity.this, ActividadVibrar.class);
                startActivity(intent);
                break;
            case R.id.opcionLinterna:
                intent = new Intent(MainActivity.this, ActividadLinterna.class);
                startActivity(intent);
                break;
        }
        return true;
    }
}
